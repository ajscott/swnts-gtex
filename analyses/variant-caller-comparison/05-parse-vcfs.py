#!/usr/bin/python

import argparse
import sys

#Set up argparse and return parser
def createParser():
	parser = argparse.ArgumentParser()
	parser.add_argument('-v', action='store', required=True, help='vcf')
	parser.add_argument('-af', action='store', required=False, help='minimum allele frequency')
	parser.add_argument('-cov', action='store', required=False, help='minimum coverage')
	return parser

def getVAF(line):
	fields = line.split("\t")
	formatField = fields[8]
	deepVariant = fields[9]
	if "VAF" in formatField:
		vafIndex = formatField.split(":").index("VAF")
		vaf = deepVariant.split(":")[vafIndex].split(",")[0]
		if vaf != ".":
			return vaf
		else:
			return None
	return None	

def getDepth(line):
	fields = line.split("\t")
	formatField = fields[8]
	deepVariant = fields[9]
	if "DP" in formatField:
		dpIndex = formatField.split(":").index("DP")
		dp = deepVariant.split(":")[dpIndex].split(",")[0]
		if dp != ".":
			return dp
		return None
	return None

def getVariantType(line):
	fields = line.split("\t")
	if len(fields[3])==1 and len(fields[4])==1:
		return "SNP"
	else:
		return "Other"

#Process input vcf
#Print vaf info to stdout
def processVCF(vcf, minaf, mincov, applyFilter):
	with open(vcf,"r") as f:
		for line in f:
			if line[0] == "#" and applyFilter:
				print(line.rstrip())
			if line[0] != "#":
				vaf = getVAF(line)
				dp = getDepth(line) typeOfVariant = getVariantType(line)	
				if vaf is not None and not applyFilter:
					print("\t".join([vcf,vaf,dp,typeOfVariant]))
				if vaf is not None and float(vaf) >= minaf and float(dp) >= mincov and applyFilter:
					print(line.rstrip())
				
	f.close()	


def main(args):
	parser = createParser()
	args = parser.parse_args()
	mincov = 0
	minaf = 0
	applyFilter = False
	if args.af is not None and args.cov is not None:
		minaf = float(args.af)
		mincov = float(args.cov)	
		applyFilter = True
	processVCF(args.v, minaf, mincov, applyFilter)

# Call main() and exit after finishing
if __name__ == '__main__':
	sys.exit(main(sys.argv))
