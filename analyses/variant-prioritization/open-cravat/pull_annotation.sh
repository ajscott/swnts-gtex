#!/bin/bash
#usage ./pull_annotators <vcf_file> <Annotation_to_pull>
#check if arguments
if [ "$#" -ne 2 ]; then
  echo "Please supply the needed arguments"
  echo "Parameters: $0 <vcf_file> <OC_Annotation_to_pull>"
  exit 1
fi

#asign arguments to variables 
vcf_file=$1
annotation_to_pull=$2

#pull out the annotations
awk -v annotation="$annotation_to_pull" 'BEGIN {OFS="\t"} {if ($0 !~ /^#/) {split($8,info,";"); var=""; for (i in info) {if (match(info[i],annotation)) {split(info[i],tmp,"="); var=var";"tmp[2]}}; printf "%s\t%s%s\n", $1, $2, var}}' $vcf_file
