FROM rocker/tidyverse:latest

RUN apt-get update -qq -y && apt-get install --no-install-recommends -y \
    bash-completion \
    dpkg-dev \
    zlib1g-dev \
    libssl-dev \
    libffi-dev \
    zlib1g-dev \
    libbz2-dev \
    liblzma-dev \
    build-essential \
    libglpk40 \
    openssh-client \
    curl \
    less \
    libcurl4-openssl-dev \
    libgsl-dev \
    python3 \
    python3-pip

RUN pip3 install synapseclient[pandas,pysftp]

RUN R -e "install.packages('BiocManager')" \
&& R -e "install.packages('ggplot2')" \
&& R -e "install.packages('reticulate')" \ 
&& R -e "install.packages('extrafont')" \
&& R -e "BiocManager::install('biomaRt')" \ 
&& R -e "BiocManager::install('limma')" \ 
&& R -e "BiocManager::install('edgeR')" \ 
&& R -e "BiocManager::install('ComplexHeatmap')" \ 
&& R -e "devtools::install_github('brian-bot/githubr')" \ 
&& R -e "devtools::install_github('Sage-Bionetworks/knit2synapse')" \ 
&& R -e "devtools::install_github('Sage-Bionetworks/sagethemes', ref = 'main')" \
&& R -e "install.packages('knitr')" \ 
&& R -e "install.packages('R.utils')" \ 
&& R -e "install.packages('utils')" \
&& R -e "install.packages('enrichR')" \
&& R -e "install.packages('pheatmap')" \
&& R -e "install.packages('Rtsne') \
&& R -e "install.packages('umap')
