Annotations and their Intepretations

1. ALoFT: Annotation of Loss-of-Function Transcripts
Annotates putative loss of Function variants in proteing-colding including functional, evolutionary and network features. Further, ALoFT can predict the impact of premature stop variants.
classifies putative loss of function variants into three classes: those that are benign, those that lead to recessive disease (disease-causing only when homozygous) and those that lead to dominant disease (disease-causing as heterozygotes).
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_aloft__transcript,Number=A,Type=String,Description="Ensembl transcript ids">
##INFO=<ID=OC_aloft__affect,Number=A,Type=String,Description="the fraction of the transcripts of the gene affected i.e. No. of transcripts affected by the SNP/Total no. of protein_coding transcripts for the gene.">
##INFO=<ID=OC_aloft__tolerant,Number=A,Type=Float,Description="Probability of the SNP being classified as benign by ALoFT">
##INFO=<ID=OC_aloft__recessive,Number=A,Type=Float,Description="Probability of the SNP being classified as recessive disease-causing by ALoFT">
##INFO=<ID=OC_aloft__dominant,Number=A,Type=Float,Description="Probability of the SNP being classified as dominant disease-causing by ALoFT">
##INFO=<ID=OC_aloft__pred,Number=A,Type=String,Description="final classification predicted by ALoFT; values can be Tolerant, Recessive or Dominant">
##INFO=<ID=OC_aloft__conf,Number=A,Type=String,Description="Confidence level of Aloft_pred; values can be "High Confidence" (p < 0.05) or "Low Confidence" (p > 0.05)">
##INFO=<ID=OC_aloft__all,Number=A,Type=String,Description="">

These can be filtered using bcftools: bcftools query -f  '%CHROM %POS %OC_aloft__tolerant %OC_aloft__recessive %OC_aloft__dominant %OC_aloft__pred %OC_aloft__conf%\n' vcf_file.vcf

2. Combined Annotation Dependent Depletion (CADD)
This is used to score the deleteriousness of single nucleotide variants as well as insertion/deletions variants in the human genome.
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_cadd_exome__score,Number=A,Type=Float,Description="">
##INFO=<ID=OC_cadd_exome__phred,Number=A,Type=Float,Description="">
The OC_cadd_exome__score: negative values, suggests extent to which the variant is neutral, Positive values suggests extent to which variant is deleterious 
The OC_cadd_exome__phred: scaled scores,  a scaled score of 10 or greater indicates a raw score in the top 10% deleterious variants in the human genome, and a score of 20 or greater indicates a raw score in the top 1% deleterious variants in the human genome.****
filtering example: bcftools query ../OC_Reports/ocweb_chr21_swn_patient_1_blood.vcf.gz -f  '%CHROM %POS %OC_cadd_exome__score %OC_cadd_exome__phred\n' 

3.FATHMM XF
 FATHMM extended - predicts whether single nucleotide variants (SNVs) are deleterious or neutral
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_fathmm_xf_coding__fathmm_xf_coding_score,Number=A,Type=Float,Description="Scores are p-values and range from 0 to 1. SNVs with scores >0.5 are predicted to be deleterious, and those <0.5 are predicted to be neutral or benign.">
##INFO=<ID=OC_fathmm_xf_coding__fathmm_xf_coding_rankscore,Number=A,Type=Float,Description="Ratio of the rank of the score over the total number of FATHMM XF scores.">
##INFO=<ID=OC_fathmm_xf_coding__fathmm_xf_coding_pred,Number=A,Type=String,Description="If score is >0.5, the corresponding nsSNV is predicted as "Damaging"; otherwise it is predicted as "Neutral".">
Predictions are given as p-values in the range [0, 1]: values above 0.5 are predicted to be deleterious, while those below 0.5 are predicted to be neutral or benign. P-values close to the extremes (0 or 1) are the highest-confidence predictions that yield the highest accuracy.
If score is >0.5, the corresponding nsSNV is predicted as "Damaging"; otherwise it is predicted as "Neutral".
**Didnt include it, due to errors when performing bcftools operations on the vcf

4. GRASP/GWAS - Genome-Wide Repository of Associations Between SNPs and Phenotypes
provides information on the phenotypes associated with the variant
##INFO=<ID=OC_grasp__nhlbi,Number=A,Type=String,Description="">
##INFO=<ID=OC_grasp__pmid,Number=A,Type=String,Description="PubMed ID">
##INFO=<ID=OC_grasp__phenotype,Number=A,Type=String,Description="">
##INFO=<ID=OC_grasp__all,Number=A,Type=String,Description="">
filtering Example: bcftools query ../OC_Reports/ocweb_chr21_swn_patient_1_blood.vcf.gz -f  '%CHROM %POS %OC_grasp__nhlbi %OC_grasp__pmid %OC_grasp__phenotype %OC_grasp__all\n'

5. GTEx: Genotype-Tissue Expression
provides correlations between genotype and tissue-specific gene expression levels
##INFO=<ID=OC_gtex__gtex_gene,Number=A,Type=String,Description="">
##INFO=<ID=OC_gtex__gtex_tissue,Number=A,Type=String,Description="">
the output is Target gene and Tissue
filtering example: 
**Didnt include it, due to errors when performing bcftools operations on the vcf

6. GWAS Catalog
provides information about the trait the variant is associated with. 
##INFO=<ID=OC_gwas_catalog__disease,Number=A,Type=String,Description="">
##INFO=<ID=OC_gwas_catalog__or_beta,Number=A,Type=Float,Description="">
##INFO=<ID=OC_gwas_catalog__pval,Number=A,Type=Float,Description="">
##INFO=<ID=OC_gwas_catalog__pmid,Number=A,Type=String,Description="">
##INFO=<ID=OC_gwas_catalog__init_samp,Number=A,Type=String,Description="">
##INFO=<ID=OC_gwas_catalog__rep_samp,Number=A,Type=String,Description="">
##INFO=<ID=OC_gwas_catalog__risk_allele,Number=A,Type=String,Description="">
##INFO=<ID=OC_gwas_catalog__ci,Number=A,Type=String,Description="">

